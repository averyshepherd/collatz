#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


def collatz_cache(k, v, cache={}):
    """
    adds k as key and v as value to a chache
    cache is blank dictionary if no cache specified
    """
    if k not in cache:
        cache[k] = v
    return cache


def init_cache(cache={}):
    """
    initializes the cache with key value pairs
    of cycle length 1-999999, with a step of 1000
    """
    for k in range(1, 1000000, 1000):
        v = collatz_length(k)
        cache = collatz_cache(k, v, cache)
    return cache


def collatz_length(n):
    """
    finds the length of a collatz cycle of one number (n)
    """
    assert n > 0
    c = 1
    while n > 1:
        if (n % 2) == 0:
            n = (n // 2)
        else:
            n = (3 * n) + 1
        c += 1
    assert c > 0
    return c


def get_max(start, end, cache):
    """
    start is the beginning of the range, inclusive
    end is the end of the range, exclusive
    this returns the max cycle length of the range
    """
    max = 1
    for k in range(start, end):
        if k in cache:		# checks to see if value is in cache
            v = cache[k]
        else:
            v = collatz_length(k)
            collatz_cache(k, v, cache)  # adds value to cache if not in cache
        if v > max:				# checks to see if the value is the largest
            max = v
    return max


def collatz_eval(i, j):
    """
i the beginning of the range, inclusive
j the end       of the range, inclusive
return the max cycle length of the range [i, j]
"""
    # initializes the cache with key value pairs
    # of cycle length 1-999999, with a step of 1000
    cache = init_cache()

    # checks order of range and makes sure that first
    # number is the larger
    if i < j:
        max = get_max(i, j+1, cache)
    elif j < i:
        max = get_max(j, i+1, cache)
    else:			# if numbers are the same only do the length of one number
        return collatz_length(i)
    return max
# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)

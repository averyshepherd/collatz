import sys
from io import StringIO

class Collatz(object):
	def __init__(self):
		self.cache = {}
	def read(self, s):
		if not s.strip():
			a = s.split()
			return [int(a[0]), int(a[1])]

	# ------------
	# collatz_eval
	# ------------
	def add_cache(self, k, v):
		if k not in self.cache:
			self.cache[k] = v

	def init_cache(self):
		for k in range(1, 1000000, 1000):
			v = self.cycle(k)
			self.add_cache(k, v)

	def cycle(self, n):
		assert n > 0
		c = 1
		while n > 1:
			if (n % 2) == 0:
				n = (n // 2)
			else :
				n = (3 * n) + 1
				c += 1
		assert c > 0
		return c

	def get_max(self, start, end):
		max = 1
		for k in range(start, end):
			if k in self.cache:
				v = self.cache[k]
			else:
				v = self.cycle(k)
				self.add_cache(k, v)
			if v > max:
				max = v
		return max

	def collatz_eval(self, i, j):
		if i < j:
			max = self.get_max(i, j+1)
		elif j < i:
			max = self.get_max(j, i+1)
		else:
			return self.cycle(i)
		return max
	# -------------
	# collatz_print
	# -------------


	def print(self, w, i, j, v):
		w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

	# -------------
	# collatz_solve
	# -------------


	def solve(self, r, w):
		for s in r:
			i, j = self.read(s)
			v = self.collatz_eval(i, j)
			self.print(w, i, j, v)



if __name__ == "__main__":
	test = Collatz()
	test.init_cache()
	print(test.collatz_eval(1, 10))
	r = StringIO("8266 8259\n7561 7567\n4786 4479\n")
	w = StringIO()
	test.solve(r, w)
	print(w.getvalue())
